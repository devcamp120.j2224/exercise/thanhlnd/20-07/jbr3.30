package com.devcamp.employee;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmployeeRestApi {
    @CrossOrigin
    @GetMapping("/employees")
    public ArrayList<Employee> employeeList(){
        ArrayList<Employee> employeeList = new ArrayList<Employee>();
        Employee employee1 = new Employee(01, "N V", "A", 10000);
        Employee employee2 = new Employee(02, "T V", "B", 2000);
        Employee employee3 = new Employee(03, "T V", "C", 3500);
        
        System.out.println(employee1);
        System.out.println(employee2);
        System.out.println(employee3);

        employeeList.add(employee1);
        employeeList.add(employee2);
        employeeList.add(employee3);
        return employeeList;   
    }
}
