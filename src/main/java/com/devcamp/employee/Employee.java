package com.devcamp.employee;

public class Employee {
    private int id;
    private String firstName;
    private String lastName;
    private int salary;
    
    public Employee(int id, String firstName, String lastName, int salary) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.salary = salary;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return firstName + " " + lastName;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public int getAnnualSalary(){
        return salary*12;
    }

    public int raiseSalary(int percent){
        this.setSalary(salary*(1+percent)/100);
        return salary;
    }

    @Override
    public String toString(){
        return String.format("Employee[id= %s, name=%s, salary= %s]", id, firstName + " " + lastName, salary); 
    }
}
